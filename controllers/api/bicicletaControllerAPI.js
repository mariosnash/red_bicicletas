var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){   
    Bicicleta.find({}, function(err, bicicleta){
        res.status(200).json({ bicicletas: bicicleta });
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta({ num_id: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]});
    Bicicleta.add(bici, function (err) {
        res.status(200).json({ bicicleta: bici });
    })
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeById(req.body.id, function() {
        res.status(204).send();
    });
}

exports.bicicleta_update = function(req, res){
    var update_values = {
        num_id: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };
    var bici = Bicicleta.findOneAndUpdate(req.params.id, update_values, function() {
        res.status(200).json({ bicicleta: update_values });
    });  
}