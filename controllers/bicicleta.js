var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.render('bicicletas/index', {bicis: bicis});
    });
}

exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res) {
    var bici = new Bicicleta({num_id: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]});
    console.log(bici);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function (req, res) {
    Bicicleta.findById(req.params.id, function(err, bici){
        res.render('bicicletas/update', {errors: {}, bici: bici});
    });
    
}

exports.bicicleta_update_post = function(req, res, next) {
    var update_values = {color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]};
    Bicicleta.findByIdAndUpdate(req.params.id, update_values, function(err, bici) {
        if (err) {
            console.log(err);
            res.render('bicicletas/update', {errors: err.errors, bici});
        }else {
            res.redirect('/bicicletas');
            return;
        }
    });
},

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.findByIdAndDelete(req.params.id, function(err) {
        if (err) {
            next(err);
        } else {
            res.redirect('/bicicletas');
        }
    });
    
}