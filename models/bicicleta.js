//---------- ESQUEMA MONGOOSE ----------//

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    num_id: String,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(num_id, color, modelo, ubicacion) {
    return new this({
        num_id: num_id,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb) { // statics significa que lo estoy agregando directo al modelo Bicicleta de mongoose.
    return this.find({}, cb); // Find trae todos las bicis porque se le pasa el parámetro de busqueda vacío "{}".
};

bicicletaSchema.statics.add = function(aBici, cb) {
    this.create(aBici, cb);
};

/*bicicletaSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({code: aCode}, cb);
};
*/
bicicletaSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({code: aCode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

//---------- FIN MONGOOSE ----------//
/*
var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.tostring = function () {
    return 'id: ' + this.id + " | color: " + this.color; 
}

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, 'Rojo', 'Scott plasma', [-25.276309, -57.624957]);
var b = new Bicicleta(2, 'Azul', 'Canyon Aeroad', [-25.287252, -57.594487]);
var c = new Bicicleta(3, 'Negro Mate', 'Scott Sub Cross 10 Men', [-25.287563,-57.609508]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);

module.exports = Bicicleta;*/