const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const TokenSchema = new Schema({
    _userId: { //Hace una referencia a la instancia del objeto Usuario. Trae todos los elementos del Usuario.
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Usuario'
    },
    token: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 43200 //Cantidad de tiempo. Se elimina cuando el token cumple el tiempo. 
    }
});

module.exports = mongoose.model('Token', TokenSchema);